'use strict';

let products = [
    {"name": "Sledgehammer",    "price": 125.75},
    {"name": "Axe",             "price": 190.50},
    {"name": "Bandsaw",         "price": 562.13},
    {"name": "Chisel",          "price": 12.9},
    {"name": "Hacksaw",         "price": 18.45}
];

const express = require('express');
const session = require('express-session');
const port = 3001;

const app = express();

let getDecimal = function(value) {
    if(value) return value.toFixed(2);
    return value;
};

let viewProduct = function({name, price}) {
    price = getDecimal(price);

    return `
    <li data-name="${name}">
        <span class="product-name">${name}</span>
        <span class="product-price">${price}</span>
        <a href="cart/add/?name=${name}">Add to the cart</a>
    </li>
    `;
};

let viewProducts = function() {
    let items = '';

    products.forEach(item => {
        items += viewProduct(item);
    });

    return `
        <ul id="product-list">${items}</ul>
    `;
};

class Cart {

    constructor(session) {
        this.session = session;
    }

    addProduct(name) {
        console.info(`Adding product ${name} to the cart`);

        let items = this.getFromSession();
        let found = items.find(item => {
            return name === item.name;
        });

        if(found) {
            found.quantity++;
        } else {
            let product = this.getProduct(name);
            if(product == null) {
                console.error('The product you\'re trying to add doesn\'t exist');
                return false;
            }

            items.push({
                name    : name,
                price   : product.price,
                quantity: 1
            });

        }

        this.saveToSession(items);
    }

    getProduct(name) {
        return products.find(item => {
            return name === item.name;
        });
    }

    removeProduct(name) {
        console.info(`Removing product  ${name} from the cart`);
        let items = this.getFromSession();
        let newItems = [];
        items.forEach(item => {
            if(item.name !== name) newItems.push(item);
        });

        this.saveToSession(newItems);
    }

    getTotal() {
        let items = this.getFromSession();
        let total = 0;

        items.forEach(item => {
            total += item.quantity * item.price;
        });

        return total;
    }

    viewCart() {
        let items = this.getFromSession();

        let cartItems = '';
        let total = getDecimal(this.getTotal());

        items.forEach(item => {
            cartItems += this.viewCartItem(item);
        });

        return `<p>Cart items: </p><ul id="cart">${cartItems}</ul><div id="total">Total: ${total}</div>`;
    }

    viewCartItem({name, price, quantity}) {

        let total = getDecimal(price * quantity);
        price = getDecimal(price);

        return `
        <li data-name="${name}">
            <span class="product-name">${name}</span>
            <span class="product-price">${price}</span>
            <span class="product-quantity">${quantity}</span>
            <span class="product-total">${total}</span>
            <a href="cart/remove/?name=${name}">Remove</a>
        </li>`;
    }

    getFromSession() {
        if(!this.session) {
            console.error('Session not found');
            return [];
        }

        this.session.cart = 'undefined' !== typeof this.session.cart ? this.session.cart: [];
        return this.session.cart;
    }

    saveToSession(items) {
        if(!this.session) {
            console.error('Session not found');
        } else {
            this.session.cart = items;
        }
    }

}

app.use(session({
    secret  : '1-cart',
    resave  : false,
    saveUninitialized: true
}));

app.get('/', function(req, res, next) {
    let cart = new Cart(req.session);
    res.setHeader('Content-Type', 'text/html');
    res.write(viewProducts());
    res.write(cart.viewCart());

    res.end();

    return next();
});

app.get('/cart/add', function(req, res, next) {
    let cart = new Cart(req.session);
    if(req.query.name) {
        cart.addProduct(req.query.name);
    }
    res.redirect('/');
});

app.get('/cart/remove', function(req, res, next) {
    let cart = new Cart(req.session);
    if(req.query.name) {
        cart.removeProduct(req.query.name);
    }
    res.redirect('/');
});

app.listen(port, () => console.log(`App listening on port ${port}!`));